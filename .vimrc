

" ############
"  GENERAL
" ############
" Copy to the system clipboard by default ("+ "* registers)
" Needs vim-gtk (no gnome dependecies) or vim-gnome
set clipboard=unnamedplus

set autoread      " Autoread when a file is modified from the outside
set backspace=indent,eol,start " Make <Backspace> delete any automatically inserted indentation
set confirm       " Ask for confirmation instead of failing commands
set hidden        " Hide buffers when changing instead of closing them
set hlsearch      " Highlight text when searching
set incsearch     " Search as characters are entered
set ignorecase    " Case insensitive search
set laststatus=2  " Always show status bar
set mouse=a       " Enable use of the mouse for all modes
set noshowmode    " Don't show the name of modes (INSERT MODE, etc.)
set noswapfile    " Didn't have a good experience with .vimtmp and swap files
set number        " Line numbers
set scrolloff=12  " Keep N spaces between cursor and screen edges
set showcmd       " Show partial vim commands at the bottom right
set showmatch     " Highlight matching brackets
set smartcase     " except when using capital letters
set ttimeoutlen=0 " Prevent delay between normal and insert modes
set wildignorecase  " Ignore casing in autocomplete and wildmenu
set wildmenu      " Show all options in command mode when pressing tab
syntax on         " Turn on syntax highlighting for many extensions

" Disable Ex mode (can't find a use for it)
nnoremap Q <Nop>

" Copy to the system clipboard by default ("+ "* registers)
" Needs vim-gtk (no gnome dependecies) or vim-gnome
set clipboard=unnamedplus

" Clipboard and Wayland
"autocmd TextYankPost * if (v:event.operator == 'y' || v:event.operator == 'd') | silent! execute 'call system("wl-copy", @")' | endif
"nnoremap p :r!wl-paste --no-newline<CR>
"nnoremap P <Up>p


" Add dictionary completion to the default insert mode completion (C-P; C-N)
" The dictionary is set automatically when spelling is set
set complete+=k

filetype indent on  " Set autoindent

" Indentation by filetype management
function! AleIndent() abort

  if &ft =~ 'vim\|html\|tex\|markdown\|text\|yaml'
    set shiftwidth=2
    set softtabstop=2
    set expandtab
    return
  endif

  if &ft =~ 'python\|sh\|R'
    set shiftwidth=4
    set softtabstop=4
    set expandtab
    return
  endif
  " All other filetypes:
  set shiftwidth=4
  set softtabstop=4
  set expandtab
endfun

augroup ApplyAleIndent
  autocmd!
  autocmd BufEnter * call AleIndent()
augroup END


" File type specific configuration
" Use $HOME/.vim/ftplugin/[FILETYPE].vim
" And use $HOME/.vim/ftdetect/[FILETYPE].vim to define what file extensions
" to recognize as a filetype
" (e.g. *.md as Markdown, in the mkd.vim file)
" If any of these directories or files don't exist, just create them
filetype plugin on


" ############
"  DIGRAPHS AND SPECIAL SYMBOLS
" ############

" See :digraphs for the possible combinations. E.g (many work also backwards)
source ~/.vim/vimrc_cont/digraphs.vim


" ############
"  APPEARANCE
" ############

" Custom status bar
" Support for vim-gui truecolor schemes
" Syntax highligh in markdown codeblocks
" Highlight trailing whitespace and spelling mistakes
" ColorScheme management
" Different cursors for normal, insert, replace modes
source ~/.vim/vimrc_cont/appearance.vim


" ############
"  PLUGINS
" ############

" Using vim-plug plugin manager: vim-plug

" Install vim-plug if not found
"   NOTE: needs curl installed on the system!
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif
" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif

" Call plugins from a centralized custom directory
" Silence in neovim
if !has('nvim')
  call plug#begin('~/.vim/plugged')
endif
if has('nvim')
  silent! call plug#begin('~/.vim/plugged')
endif

" jedi autocompletion for python
"Plug 'davidhalter/jedi-vim', { 'for': 'python'}
" coc.nvim for python
Plug 'neoclide/coc.nvim', { 'branch': 'release', 'for': 'python'}
" Colors the background of color codes #ffffff
Plug 'ap/vim-css-color'
" Markdown custom tools (made by me) dependency: ~/.vim/autoload/mdtools.vim
Plug 'godlygeek/tabular', { 'for': 'markdown'}
" Manage clipboard in wayland desktops
Plug 'jasonccox/vim-wayland-clipboard'
call plug#end()

" Plugins general config
let g:coc_disable_startup_warning = 1
" Disable suggestions popup
let b:coc_suggest_disable = 1

"vim-latex-live-preview
"autocmd Filetype tex setl updatetime=1000 " Preview update every 1s
"let g:livepreview_previewer = 'mupdf'   " Set live preview to mupdf
"let g:livepreview_previewer = 'zathura'   " Use Zathura as default preview


" ############
" KEYMAPPINGS
" ############

source ~/.vim/vimrc_cont/keymappings.vim


" ############
" CUSTOM TEXT OBJECTS
" ############

" '[l]ine since first non-blank character' text object
xnoremap il g_o^
onoremap il :normal vil<CR>
xnoremap al $o^
onoremap al :normal val<CR>

" 'Full [L]ine' text object
xnoremap iL g_o0
onoremap iL :normal ViL<CR>
xnoremap aL $o0
onoremap aL :normal VaL<CR>

" 'Contents of buffer' text object
"xnoremap i% GoggV
"onoremap i% :normal vi%<CR>


" ############
" CUSTOM COMMANDS
" ############

" Silence external commands and avoid the need of redrawing the screen
" This is used many times in various ftplugin files, DON'T CHANGE IT
" First version: command! -nargs=1 Silent execute 'silent !' . <q-args> | execute 'redraw!'
command! -nargs=+ Silent execute 'silent <args>' | redraw! | nohl

