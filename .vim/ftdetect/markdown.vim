" Force vim to interpret these extensions as Markdown
autocmd! BufNewFile,BufFilePre,BufRead *.markdown set filetype=markdown
autocmd! BufNewFile,BufFilePre,BufRead *.md       set filetype=markdown
