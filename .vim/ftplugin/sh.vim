" Shell/Bash filetype specific configuration
" Note: May override global .vimrc configuration
" Use `setlocal` instead of `set`!

" Reset configuration
noremap <Leader>v :source ~/.vimrc<CR>:source ~/.vim/ftplugin/sh.vim<CR>:nohl<CR>:redraw!<CR>


"""
" FOLDING:
"""
setlocal foldenable         " Enable code folding
setlocal foldmethod=indent  " Fold based on indentation
setlocal foldlevel=10
setlocal foldlevelstart=10  " Start with medium and big folds closed

" Open/close folds
"noremap <Leader><Space> za
"noremap <Leader>o zR
"noremap <Leader>c zA


"""
" BRACKETS:
"""
" Auto-complete opening parentheses
inoremap ( (<Space><Space>)<Left><Left>
inoremap [ [<Space><Space>]<Left><Left>
inoremap {<Space> {}<Left>
inoremap {<CR> {<CR><CR>}<Up><TAB>

" Navigate to the next unmatched bracket
nnoremap z( ](
nnoremap z[ ][
nnoremap z{ ]{


"""
" KEYBOARD SHORTCUTS:
"""
" Execute the file in a small vim terminal on the bottom (no args)
noremap <F5> :let $CURRENT_FILE=expand('%')<CR>:split<CR><C-W>j<ESC>:terminal<CR><C-W>j<ESC>:q<CR><C-W>:resize -10<CR><C-C>./"${CURRENT_FILE}"<CR>
" Original
"noremap <F5> :! bash "%"<CR>

" Execute the file in a small vim terminal on the bottom (no args)
map <Leader>x :let $CURRENT_FILE=expand('%')<CR>:split<CR><C-W>j<ESC>:terminal<CR><C-W>j<ESC>:q<CR><C-W>:resize -10<CR><C-C>./"${CURRENT_FILE}"<Space>
" Original
"map <Leader>x :! bash "%"<Space>


"""
" PLUGIN CONFIG:
"""

" Enable popup suggestions
let b:coc_suggest_disable = 0

