" Python filetype specific configuration
" Note: May override global .vimrc configuration
" Use `setlocal` instead of `set`!

" Reload config file
nnoremap <Leader>v :source ~/.vimrc<CR>:source ~/.vim/ftplugin/python.vim<CR>:nohl<CR>:redraw!<CR>

"""
" BOILERPLATE:
"""
nnoremap <Leader>b :r! cat ~/.vim/boilerplate/python<CR>ggddA<CR>


"""
" FOLDING:
"""
setlocal foldenable         " Enable code folding
setlocal foldmethod=indent  " Fold based on indentation
setlocal foldlevel=10
set foldlevelstart=10  " Start with medium and big folds closed


"""
" BRACKETS:
"""
" Auto-complete opening parentheses
inoremap ( ()<Left>
inoremap [ []<Left>
inoremap { {}<Left>


"""
" KEYBOARD SHORTCUTS:
"""
" Execute the file in a small vim terminal on the bottom (no args)
nnoremap <F5> :let $CURRENT_FILE=expand('%')<CR>:split<CR><C-W>j<ESC>:terminal<CR><C-W>j<ESC>:q<CR><C-W>:resize -10<CR><C-C>./"${CURRENT_FILE}"<CR>
" Original
"noremap <F5> :! python3 "%"<CR>

" Execute the file in a small vim terminal on the bottom (no args)
nnoremap <Leader>x :let $CURRENT_FILE=expand('%')<CR>:split<CR><C-W>j<ESC>:terminal<CR><C-W>j<ESC>:q<CR><C-W>:resize -10<CR><C-C>./"${CURRENT_FILE}"<Space>
" Original
"map <Leader>x :! python3 "%"<Space>


"""
" PLUGIN CONFIGURATION:
"""

"" jedi:
"" Don't mess with the vim config
""let g:jedi#auto_vim_configuration = 0
""Don't complete when pressing '.'
""let g:jedi#popup_on_dot = 0
"" Show help for function contents in (1) weird pop-up, (2) the command line
"let g:jedi#show_call_signatures = 2
"" Auto-complete when importing sub-modules 'from X import ...'
""let g:jedi#smart_auto_mappings = 1
""" Key mappings
""let g:jedi#goto_command = "<leader>d"
""let g:jedi#goto_assignments_command = "<leader>g"
""let g:jedi#goto_stubs_command = ""
""let g:jedi#goto_definitions_command = ""
""let g:jedi#documentation_command = "K"
""let g:jedi#usages_command = "<leader>u"
""let g:jedi#completions_command = "<C-Space>"
""let g:jedi#rename_command = "<leader>r"

"setlocal cmdheight=2
"set hidden
"set nobackup
"set nowritebackup
"set shortmess=""

" Enable popup suggestions
let b:coc_suggest_disable = 0

" Activate autocomplete with C-Space
inoremap <silent><expr> <c-@> coc#refresh()


" coc.nvim
" Automatically download coc-python if missing
"let g:coc_global_extensions = ['coc-python']

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("patch-8.1.1564")
  " " Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> <Leader>k :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction

augroup mygroup
  autocmd!
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Remap <C-f> and <C-b> for scroll float windows/popups.
if has('nvim-0.4.0') || has('patch-8.2.0750')
  nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
  inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
  inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
  vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
endif

