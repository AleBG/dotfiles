" ############
"  STATUS BAR
" ############

set statusline =
set statusline +=[b:%02n]           " buffer number
"set statusline +=[%{&ff}]\         " file format (dos, unix, etc.)
set statusline +=%y                 " file type
"set statusline +=\[%{''.(&fenc!=''?&fenc:&enc).''},    "Encoding
"set statusline +=%{&spelllang}]    " spell language
set statusline +=\ %<%F             " full path
set statusline +=%m%r               " modified, read-only flags
set statusline +=%=\ [\l:%03l/%03L  " current line
set statusline +=%=\ \c:%03v]       " virtual column number

" Minimal original line
"set statusline=%F\ \ \ \ \ \ Line\ %l\ of\ %L\ \ \ \ Column:\ %c


" ############
"  VIM-GUI TRUECOLOR
" ############

" Support for vim-gui truecolor schemes
if (has("termguicolors"))
  set termguicolors
endif


" ############
"  SYNTAX HIGHLIGHTING IN MARKDONW CODEBLOCKS
" ############

" This has to go here (not in .../ftplugin), otherwise it doesn't work
let g:markdown_fenced_languages=['python', 'py=python', 'r', 'R=r', 'sql', 'SQL=sql', 'bash=sh', 'sh', 'yaml', 'javascript', 'js=javascript', 'html', 'css', 'vim', 'json', 'diff', 'tex']


" ############
"  COLORSCHEME MANAGEMENT FOR HIGHLIGHTING TRAILING WHITESPACE AND SPELLING MISTAKES
" ############

" Highlight trailing whitespace and spelling mistakes
" Highlight best practice (in order):
" (1) a function
function! HLWhiteSpace() abort
  " Show trailing whitespace except when inserting at end of line
  highlight ExtraWhitespace ctermbg=darkred guibg=darkred
  match ExtraWhitespace /\s\+$/
endfunction

function! HLSpellingMistakes() abort
  " HL spelling mistakes
  " (only works when set spell + set spelllang=...)
  highlight clear SpellBad
  " cterm is for terminal vim, gui for gvim
  highlight SpellBad cterm=underline ctermfg=red
  highlight SpellBad gui=undercurl guifg=#af0000
endfunction

" (2) augroup w/ autocmd
augroup CallAleHighlights
  " Remove autocmds from this group
  autocmd!
  " Forces custom Highlights with autocommands
  autocmd ColorScheme * call HLWhiteSpace()
  autocmd ColorScheme * call HLSpellingMistakes()
augroup END

" (3) ColorScheme management
function! AleColors() abort
  if &ft =~ 'vim'
    " soft, medium, or hard
    let g:gruvbox_contrast_light = 'medium'
    setlocal background=light
    colorscheme gruvbox
    call HLWhiteSpace()
    call HLSpellingMistakes()
    return
  endif
  if &ft =~ 'markdown\|text\|yaml\|json\|tex'
    let g:gruvbox_contrast_light = 'hard'
    setlocal background=light
    colorscheme gruvbox
    call HLWhiteSpace()
    call HLSpellingMistakes()
    return
  endif
  if &ft =~ 'python\|pyrex\|sh\|R'
    let g:gruvbox_contrast_dark = 'hard'
    setlocal background=dark
    colorscheme gruvbox
    call HLWhiteSpace()
    call HLSpellingMistakes()
    return
  endif
  " All other filetypes:
  colorscheme white-sand
endfun

augroup ApplyAleColors
  autocmd!
  autocmd VimEnter * call AleColors()
augroup END


" ############
" CURSORS FOR NORMAL, INSERT, REPLACE MODES
" ############

let &t_EI = "\e[1 q"
let &t_SI = "\e[5 q"
let &t_SR = "\e[3 q"

