" ############
"  REMAPPINGS
" ############

" Vertical movement off visual lines
noremap k gk
noremap j gj
inoremap <Down> <C-o>gj
inoremap <Up> <C-o>gk

" More convenient shortcut to navigate spelling mistakes
nnoremap zl ]szz
nnoremap zh [szz
" Change the last spelling mistake with the first suggestion and come back
nnoremap zH [s1z=<C-O>

" Map Y to act like D and C, i.e. to yank until EOL
" rather than act as yy, which is the default
nnoremap Y y$

" Indent visual selection and keep the last selection
xnoremap <Tab> >gv
xnoremap <S-Tab> <gv

" Discard (close) active buffer, without closing the active window
" <bar> is '|', used to pipe commands as in the shell
nnoremap <C-b><C-c> :bp<bar>sp<bar>bn<bar>bd<CR>

" Quality of life
cnoremap W w
cnoremap Q q
cnoremap jj <Esc>
inoremap jj <Esc>
nnoremap ñ ;
" Map g- as an alias for g;, to navigate backwards to edited positions
" of the same file, so it's different from <C-O> and <C-I>
" Use g, to navigate forwards
nnoremap g- g;

" Navigation to First/Last non-white character of line
nnoremap <C-l> g_<Right>
nnoremap <C-h> ^

" Center screen after some navigation commands
nnoremap G Gzz
nnoremap <C-U> <C-U>zz
" zz at the beginning to center at EOF
nnoremap <C-D> zz<C-D>zz
nnoremap <C-O> <C-O>zz
nnoremap <C-I> <C-I>zz
nnoremap o o<Esc>zzi
nnoremap O O<Esc>zzi

" Insert mode movement using CTRL
"inoremap <C-k> <C-O>gk
"inoremap <C-j> <C-O>gj
inoremap <C-h> <Left>
inoremap <C-l> <Right>


" Surround words in normal mode (repeatable with .)
" Disable s in normal mode, it's the same as cl
nnoremap s <Nop>
" Normal ones use ciw
nnoremap s( ciw(<C-R><C-O>")<ESC>
nnoremap s[ ciw[<C-R><C-O>"]<ESC>
nnoremap s{ ciw{<C-R><C-O>"}<ESC>
nnoremap s" ciw"<C-R><C-O>""<ESC>
nnoremap s' ciw'<C-R><C-O>"'<ESC>
nnoremap s` ciw`<C-R><C-O>"`<ESC>
nnoremap sb ciw*<C-R><C-O>"*<ESC>
nnoremap s* ciw*<C-R><C-O>"*<ESC>
nnoremap sm ciw$<C-R><C-O>"$<ESC>
nnoremap s$ ciw$<C-R><C-O>"$<ESC>
nnoremap s\| ciw\|<C-R><C-O>"\|<ESC>
" Declare redundant ciw ones just in case I change the normal ones
nnoremap sw( ciw(<C-R><C-O>")<ESC>
nnoremap sw[ ciw[<C-R><C-O>"]<ESC>
nnoremap sw{ ciw{<C-R><C-O>"}<ESC>
nnoremap sw" ciw"<C-R><C-O>""<ESC>
nnoremap sw' ciw'<C-R><C-O>"'<ESC>
nnoremap sw` ciw`<C-R><C-O>"`<ESC>
nnoremap swb ciw*<C-R><C-O>"*<ESC>
nnoremap sw* ciw*<C-R><C-O>"*<ESC>
nnoremap swm ciw$<C-R><C-O>"$<ESC>
nnoremap sw$ ciw$<C-R><C-O>"$<ESC>
nnoremap sw\| ciw\|<C-R><C-O>"\|<ESC>
" Allow also for ciW ones for outer surround
nnoremap sW( ciW(<C-R><C-O>")<ESC>
nnoremap sW[ ciW[<C-R><C-O>"]<ESC>
nnoremap sW{ ciW{<C-R><C-O>"}<ESC>
nnoremap sW" ciW"<C-R><C-O>""<ESC>
nnoremap sW' ciW'<C-R><C-O>"'<ESC>
nnoremap sW` ciW`<C-R><C-O>"`<ESC>
nnoremap sWb ciW*<C-R><C-O>"*<ESC>
nnoremap sW* ciW*<C-R><C-O>"*<ESC>
nnoremap sWm ciW$<C-R><C-O>"$<ESC>
nnoremap sW$ ciW$<C-R><C-O>"$<ESC>
nnoremap sW\| ciW\|<C-R><C-O>"\|<ESC>

" Surround arbitrary strings in visual mode (Not repeatable)
" For a repeatable ('.') action do:
" [action][scope][object][character]<C-R>-[character]<Esc>
" E.g: ciw'<C-R><C-O>"'<Esc>  ciW(<C-R>-)<Esc>  ciW"<C-R>-"<Esc>
" Works with custom text objects: cil'<C-R>-'<Esc>( )
xnoremap s( <Esc>`>a)<Esc>`<i(<Esc><Right>vt)<Esc>
xnoremap s) <Esc>`>a)<Esc>`<i(<Esc><Right>vt)<Esc>
xnoremap s[ <Esc>`>a]<Esc>`<i[<Esc><Right>vt]<Esc>
xnoremap s] <Esc>`>a]<Esc>`<i[<Esc><Right>vt]<Esc>
xnoremap s{ <Esc>`>a}<Esc>`<i{<Esc><Right>vt}<Esc>
xnoremap s} <Esc>`>a}<Esc>`<i{<Esc><Right>vt}<Esc>
xnoremap s" <Esc>`>a"<Esc>`<i"<Esc><Right>vt"<Esc>
xnoremap s' <Esc>`>a'<Esc>`<i'<Esc><Right>vt'<Esc>
xnoremap s` <Esc>`>a`<Esc>`<i`<Esc><Right>vt`<Esc>
xnoremap s* <Esc>`>a*<Esc>`<i*<Esc><Right>vt*<Esc>
xnoremap s\| <Esc>`>a\|<Esc>`<i\|<Esc><Right>vt\|<Esc>
xnoremap ss <Esc>`>a<Space><Esc>`<i<Space><Esc>E
" Delete whatever is surrounding the visual selection
xnoremap sx <Esc>`>x`<x<Esc>gv<Left><Left><Esc>


" ############
"  FILETYPE SPECIFIC
" ############
"
function! AleMappings() abort

  if &ft =~ 'markdown\|tex'
    " Navigation in the location list (lopen; mdtools#Toc(); etc.)
    nnoremap <C-k> :lprev<CR>zz<CR>
    nnoremap <C-j> :lnext<CR>zz<CR>
    nnoremap [[ :lprev<CR>zz<CR>
    nnoremap ]] :lnext<CR>zz<CR>
    " Surround for LaTeX math mode
    xnoremap s$ <Esc>`>a$<Esc>`<i$<Esc>f$
    xnoremap sm <Esc>`>a$<Esc>`<i$<Esc>f$
    xnoremap sn <Esc>`>a$<Esc>`<i$<Esc>f$
    inoremap ,sm <Esc>viW<Esc>`>a$<Esc>`<i$<Esc>Ea
    inoremap ,ms <Esc>viW<Esc>`>a$<Esc>`<i$<Esc>Ea
    inoremap ;sm <Esc>viW<Esc>`>a$<Esc>`<i$<Esc>Ea
    inoremap ;ms <Esc>viW<Esc>`>a$<Esc>`<i$<Esc>Ea
    inoremap ,sn <Esc>viW<Esc>`>a$<Esc>`<i$<Esc>Ea
    inoremap ,ns <Esc>viW<Esc>`>a$<Esc>`<i$<Esc>Ea
    inoremap .sm <Esc>viW<Esc>`>a$<Esc>`<i$<Esc>Ea
    inoremap .ms <Esc>viW<Esc>`>a$<Esc>`<i$<Esc>Ea
    " Inline math
    inoremap ,mm $$<Left>
    " Escaped curly braces for math mode
    inoremap ,cur \{<Space><Space>\}<Left><Left><Left>
    inoremap .cur \{<Space><Space>\}<Left><Left><Left>

    return
  endif

  if &ft =~ 'python\|sh'
    " Navigation in the quickfix
    nnoremap <C-k> :cprev<CR>zz<CR>
    nnoremap <C-j> :cnext<CR>zz<CR>
    nnoremap <Leader>f :copen<CR>
    return
  endif

  " All other filetypes:
  "
endfun

augroup ApplyAleMappings
  autocmd!
  autocmd BufWinEnter * call AleMappings()
augroup END


" ############
"  LEADER KEY MAPPINGS
" ############

" Leader is the space key
nnoremap <Space> <Nop>
let mapleader = " "
let g:mapleader = " "
" Disabling annoying crap (<C-Space> is not read, only <C-@>)
nnoremap <C-@> <Nop>
inoremap <C-@> <Nop>

" Clear search hl; Toogle relative number lines
nmap <Leader>c :nohl<CR>:redraw!<CR>
"nmap <Leader>n :set number! relativenumber!<CR>

" Force custom highlightings (for debuggin highlightings)
nmap <Leader>0 :call HLWhiteSpace()<CR>:call HLSpellingMistakes()<CR>zz

" Horizontal small split with a terminal
" (I never use this, it's cumbersome; better use another terminal window)
"nmap <Leader>t :split<CR><C-W>j<ESC>:terminal<CR><C-w>j<ESC>:q<CR><C-W>:resize -13<CR>
" Vim-terminal normal mode; quick closing vim-term
tnoremap <Leader>n <C-W>N
tnoremap <Leader>q <C-W>N:q!<CR>

" Delete trailing whitespaces at the end of lines
nmap <Leader>w :%s/\s\+$//g<CR>

" Show the command to replace the last yanked text + 'c'onfirmation
" .,$s makes it start from the cursor, not from the beggining of the file
nmap <Leader>r :.,$s/<C-R>0//gc<Left><Left><Left>

