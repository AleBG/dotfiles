Tilix uses dconf for its configuration.

To export the whole configuration and import it, use:
```bash
# Export active profile
dconf dump /com/gexperts/Tilix/ > tilix-alegruvd.dconf
# Import a profile
dconf load /com/gexperts/Tilix/ < tilix-alegruvd.dconf
```

- Dependencies:
    + `fonts-firacode` (in `apt`; it's the font used in the config)

Bookmarks and custom themes only are stored at `~/.config/tilix`.
