# Gruvbox colors
set $foreground #FBF1C7
set $background #201C1B
set $cursorColor #C6C6C6
set $black1 #2E3436
set $black2 #444444
set $trueblack #000000
set $red1 #FB4839
set $red2 #CC241D
set $debian_red #d70751
set $green1 #869819
set $green2 #9CBB26
set $yellow1 #FABC2F
set $yellow2 #D79920
set $blue1 #5E756C
set $blue2 #458588
set $purple1 #D3869B
set $purple2 #B16185
set $turquoise1 #7BC08B
set $turquoise2 #689D84
set $white1 #4E4E4E
set $white2 #ffffff
set $grey1 #EBDBB2
set $grey2 #A89984

# class                 border  backgr. text    indicator child_border
client.focused          #B16185 #d70751 #ffffff #CC241D   #B16185
client.focused_inactive #A89984 #5f676a #ffffff #484e50   #5f676a
client.unfocused        #A89984 #201C1B #EBDBB2 #A89984   #201C1B
client.urgent           #A89984 #CC241D #ffffff #CC241D   #CC241D
client.placeholder      #000000 #201C1B #ffffff #000000   #0c0c0c
client.background       #ffffff


######
# DEFAULT
######
# get color from XResource config - variable_name XResource fallback
set_from_resource $foreground foreground #C6C6C6
set_from_resource $background background #1C1C1C
set_from_resource $cursorColor cursorColor #C6C6C6
set_from_resource $black1 color0 #303030
set_from_resource $black2 color8 #444444
set $trueblack #000000
set $debian_red #d70751
set_from_resource $red1 color1 #d75f5f
set_from_resource $red2 color9 #d75f5f
set_from_resource $green1 color2 #87AF87
set_from_resource $green2 color10 #87AF87
set_from_resource $yellow1 color3 #ffd75f
set_from_resource $yellow2 color11 #ffd75f
set_from_resource $blue1 color4 #87afd7
set_from_resource $blue2 color12 #87afd7
set_from_resource $cyan1 color5 #afafd7
set_from_resource $cyan2 color13 #afafd7
set_from_resource $cyan1 color6 #afd7ff
set_from_resource $cyan2 color14 #afd7ff
set_from_resource $white1 color7 #4E4E4E
set_from_resource $white2 color15 #ffffff

# class                 border  backgr. text    indicator child_border
client.focused          #4c7899 #285577 #ffffff #2e9ef4   #285577
client.focused_inactive #333333 #5f676a #ffffff #484e50   #5f676a
client.unfocused        #333333 #222222 #888888 #292d2e   #222222
client.urgent           #2f343a #900000 #ffffff #900000   #900000
client.placeholder      #000000 #0c0c0c #ffffff #000000   #0c0c0c

client.background       #ffffff
