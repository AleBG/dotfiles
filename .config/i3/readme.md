
# Dependencies of this config

- `feh`: manages wallpaper image

- `fonts-font-awesome`: icon fonts for the status bar
    + Check [the official website](https://fontawesome.com/v4.7/cheatsheet/)

- `dropbox`

- `tilix`

- For screenshots:
    + `mate-screenshot` (mate)
    + `spectacle` (KDE)

- `ssh` (by default in most distros+DE)
    + Check which keys get loaded with `exec` in the i3 config file!
