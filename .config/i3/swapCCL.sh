#!/usr/bin/env bash

# Swap Caps_Lock and Control_L
#     From man xmodmap
#     Assuming $DISPLAY is ":0"

xmodmap ~/.config/i3/swapCCL -display :0
