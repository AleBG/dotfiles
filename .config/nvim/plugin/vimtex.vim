" Configuration of the vim-tex plugin
" These will load before VimTeX

" Disable vim-tex viewer; do it from outside this container
let g:vimtex_view_enabled = 0

" Disable the quickfix window opening when there are warnings
" LaTeX warnings are useless and annoying
let g:vimtex_quickfix_open_on_warning = 0

" Use XeLaTeX as the default pdf compiler engine (using latexmk)
let g:vimtex_compiler_latexmk_engines = {
  \ '_'                : '-xelatex',
  \}

" Ignore spelling errors in TeX comments
let g:vimtex_syntax_nospell_comments = 1
