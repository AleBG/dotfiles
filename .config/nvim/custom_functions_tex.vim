function! Sequence(...)
  " Returns a string of the form 'x₀, x₁, …, xₙ'
  " Use with :put =Sequence() to dump the return value into the file
  " Default output: x₀, x₁, …, xₙ
  " Pass as input a single strings of the form:
    " Note the use of the ' quotemarks, NOT the double ones
    " Sequence('x0n') => x₀, x₁, …, xₙ
    " Sequence('a0m') => a₀, a₁, …, aₘ
    " Sequence('b3i') => b₃, b₄, …, bᵢ
    " Sequence('g2z') => g₂, g₃, …, g_z
    " ...etc.
    " The 1st char will be used as the variable
    " The 2nd char as the start subindex
      " HAS TO BE a number from 0 to 8, CANNOT be 9
    " The 3rd char as the end subindex
      " If it is one of 'ijknm', we obtain, correspondingly, 'ᵢⱼₖₙₘ'
      " Otherwise, if it is another <char>, we obtain '_<char>'
    " If more than 3 chars are passed, from the 4th onward will be ignored
    " If more than one input is passed, from the second onward will be ignored


  " === SETUP ===
  " Dictionary to obtain the appropriate subscripts
  let l:subs = {"0": "₀", "1": "₁", "2": "₂", "3": "₃", "4": "₄", "5": "₅", "6": "₆", "7": "₇", "8": "₈", "9": "₉", "n": "ₙ", "m": "ₘ", "i": "ᵢ", "j": "ⱼ", "k": "ₖ"}

  " === INPUT HANDLING ===
  " Set default value
  let l:default = 'x0n'

  " Catch the first input only; if no input, use the default
  let l:instr = get(a:, 1, l:default)

  " Fix the input to a list of 3 characters in all cases, respecting the default
  if len(l:instr) == 0
    let l:in = [l:default[0], l:default[1], l:default[2]]
  elseif len(l:instr) == 1
    let l:in = [l:instr, l:default[1], l:default[2]]
  elseif len(l:instr) == 2
    let l:in = [l:instr[0], l:instr[1], l:default[2]]
  elseif len(l:instr) >= 3
    let l:in = [l:instr[0], l:instr[1], l:instr[2]]
  endif

  " === OUTPUT CONSTRUCTION ===
  " Main variable of the sequence
  let l:var = l:in[0]

  " First subscript: take the corresponding subscript from the dictionary
  let l:s0 = l:subs[l:in[1]]

  " Second subscript: compute by adding the first +1, and from the dictionary
  let l:s1 = l:subs[string(str2nr(l:in[1]) + 1)]

  " Ending subscript
  try  " If it is in the dictionary, use the UNICODE subscript
    let l:sn = l:subs[l:in[2]]
  catch  " Otherwise, just use the '_<char>' TeX syntax
    let l:sn = "_" . l:in[2]
  endtry

  " === RETURN VALUE ===
  " Concatenate everything
  return l:var . l:s0 . ", " . l:var . l:s1 . ", …, " . l:var . l:sn

endfun

