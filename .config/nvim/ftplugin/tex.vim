" TeX filetype specific configuration
" Note: May override global init.vim configuration
" Use `setlocal` instead of `set`!

" Reload config file
nnoremap <Leader>v :source ~/.config/nvim/init.vim<CR>:source ~/.config/nvim/ftplugin/tex.vim<CR>:nohl<CR>:redraw!<CR>

" Source custom functions for markdown
source ~/.config/nvim/custom_functions_tex.vim

"""
" SPELLING AND DICTIONARIES:
"""
setlocal spell                    " Set dictionary
setlocal spelllang=en,es,it,fr    " Set languages


"""
" BOILERPLATE:
"""
" Image boilerplate code
"map <Leader>i i![]<Right>(img/)<C-O>${ width=100% <C-O>0<Esc>f/
" Personal note
"nnoremap <Leader>n A<Space>\<CR>`>><Space>`\<Left><Left>


"""
" KEYBOARD SHORTCUTS:
"""
" Note: many use the custom vim command 'Silent'


" Emulate Ctrl + b/i for bold/italics of selections
" NOTE: Ctrl + i is THE SAME as <TAB> for terminals!!
xnoremap <C-b> <Esc>`>a*<Esc>`<i*<Esc><Right>vt*
inoremap <C-b> **<Left>

" Use a custom function to dump text sequences of the form x₀, x₁, …, xₙ
" DEPENDENCY: the function 'Sequence', at
" ~/.vim/vimrc_cont/custom_functions_md.vim
inoremap <C-S> <C-R>=Sequence('')<Left><Left>

" Auto-completion
inoremap { {}<Left>

