funct! Exec(command)
  " Use with :put =Exec('command') to dump the return value into the file

  redir =>output
  silent exec a:command
  redir END
  return output
endfunct!
