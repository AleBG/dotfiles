;; .emacs.d/init.el

;; ===================================
;; MELPA Package Support
;; ===================================
;; Enables basic packaging support
(require 'package)

;; Adds the Melpa archive to the list of available repositories
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.org/packages/") t)

;; Initializes the package infrastructure
(package-initialize)

;; If there are no archived package contents, refresh them
(when (not package-archive-contents)
  (package-refresh-contents))

;; Installs packages
;;
;; myPackages contains a list of package names
(defvar myPackages
  '(use-package        ;; The only package to be managed this way. For the rest, see below
    )
  )

;; Scans the list in myPackages
;; If the package listed is not already installed, install it
(mapc #'(lambda (package)
	  (unless (package-installed-p package)
	    (package-install package)))
      myPackages)

;; ====================================
;; Configuration of use-package
;; ====================================
;; This is only needed once, near the top of the file
(eval-when-compile
;; Following line is not needed if use-package.el is in ~/.emacs.d
;;   (add-to-list 'load-path "<path where use-package is installed>")
  (require 'use-package))

;; The ":ensure t" keyword causes the package(s) to be installed automatically if not already present on your system
;; Enable use-package-always-ensure if you wish this behavior to be global for all packages (comment these two lines to disable):
(require 'use-package-ensure)
(setq use-package-always-ensure t)


;; ===use-package further config==
;; If you want to keep your packages updated automatically, one option is to use auto-package-update:
;; (use-package auto-package-update
;;   :config
;;   (setq auto-package-update-delete-old-versions t)
;;   (setq auto-package-update-hide-results t)
;;   (auto-package-update-maybe))

;; When multiple hooks should be applied, the following are also equivalent:
;; (use-package ace-jump-mode
;;   :hook (prog-mode text-mode))

;; ====================================
;; Package Management with use-package
;; ====================================

;; ===magit: git-emacs integration===
(use-package magit)

;; Key binding for magit
(global-set-key (kbd "C-x g") 'magit-status)

;;
;; ====== Python IDE ======
;; The package is "python" but the mode is "python-mode". Use with python3:
(use-package python
;; Read all .py\\ extensions with python-mode
  :mode ("\\.py\\'" . python-mode)
;; Further config
  :config
;; Change interpreter to python3
  (setq python-shell-interpreter "python3")
;; Configure virtualenv
;;   (setq python-environment-virtualenv
;; 	(append python-environment-virtualenv
;; 		'("--python3" "/usr/bin/python3")))
;;
;; Load a theme in python-mode
;;   (load-theme 'material t)    ;; material-theme for python-mode
;;
;; Set default dark-theme in python mode
  (set-background-color "black")
  (set-foreground-color "white")
)

;;===python-mode hooks===
;; Auto-completition
(use-package jedi
  :config
  (add-hook 'python-mode-hook 'jedi:setup)
  (setq jedi:use-shortcuts t))

(use-package py-autopep8
  :config
  (add-hook 'python-mode-hook 'py-autopep8-enable-on-save))

;;===elpy for Python IDE on emacs===
;; Defer and init so it inits when python-mode is activated
(use-package elpy
  :defer t
  :init
  (advice-add 'python-mode :before 'elpy-enable)
)

;; elpy hooks
;; syntax checker
(use-package flycheck
  :config
  (add-hook 'elpy-mode-hook 'flycheck-mode))


;; ===================================
;; Personal Information
;; ===================================

(setq user-full-name "Luis Alejandro Bordo García"
		user-mail-address "bgluiszz@gmail.com")

;; ===================================
;; Basic Customization: Quality of Life
;; ===================================

;; Hide the startup and scratch messages
(setq inhibit-startup-message t)
(setq initial-scratch-message nil)

;; Enable line numbers globally
(global-linum-mode t)

;; Turn on matching parens highlighting
(show-paren-mode 1)

;; Turn on line wrapping everywhere
(global-visual-line-mode 1)

;; Cleans up unnecessary whitespaces on save
(add-hook 'before-save-hook 'whitespace-cleanup)

;; Remove text in active region if inserting text
(delete-selection-mode 1)

;; Auto-close brackets and double quotes
(electric-pair-mode 1)

;; Answering just 'y' or 'n' will do
(defalias 'yes-or-no-p 'y-or-n-p)

;; UTF-8
(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

;; Scrolling is smoother and I can scroll with the mouse if I need to
(setq scroll-step 1)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1)))
(setq mouse-wheel-progressive-speed nil)
(setq mouse-wheel-follow-mouse 't)
(setq redisplay-dont-pause t)



;; User-Defined init.el ends here
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages (quote (magit py-autopep8 use-package material-theme elpy))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
